# Changelog

## [1.5.4](https://gitlab.com/existe_deja/gitlab-workflow/compare/v1.5.3...v1.5.4) (2021-08-25)


### Bug Fixes

* DOCKER_HOST communication ([3c5fc2e](https://gitlab.com/existe_deja/gitlab-workflow/commit/3c5fc2e6fb5b4eff6e1738bbcd1314db2dc3b1c5))
* get back DOCKER_HOST ci variable ([4845a53](https://gitlab.com/existe_deja/gitlab-workflow/commit/4845a537c000639d33a997862022585fa1e0a051))

## [1.5.3](https://gitlab.com/existe_deja/gitlab-workflow/compare/v1.5.2...v1.5.3) (2021-08-25)


### Bug Fixes

* remove unnecessary package.json scripts ([aa580f2](https://gitlab.com/existe_deja/gitlab-workflow/commit/aa580f255f3a2b170a7a5ee2f8cba6f054e0bc66))

## [1.5.2](https://gitlab.com/existe_deja/gitlab-workflow/compare/v1.5.1...v1.5.2) (2021-08-25)


### Bug Fixes

* docker file package-lock copy ([00d9855](https://gitlab.com/existe_deja/gitlab-workflow/commit/00d9855bc74bf4937c3c55ab58df233f38ffb533))

## [1.5.1](https://gitlab.com/existe_deja/gitlab-workflow/compare/v1.5.0...v1.5.1) (2021-08-25)

# [1.5.0](https://gitlab.com/existe_deja/gitlab-workflow/compare/v1.4.1...v1.5.0) (2021-08-25)


### Features

* code update to test caching policies ([df76df6](https://gitlab.com/existe_deja/gitlab-workflow/commit/df76df6bbde3eaebdd41375223ef76b514c51773))

## [1.4.1](https://gitlab.com/existe_deja/gitlab-workflow/compare/v1.4.0...v1.4.1) (2021-08-25)

# [1.4.0](https://gitlab.com/existe_deja/gitlab-workflow/compare/v1.3.6...v1.4.0) (2021-08-25)


### Features

* code update test ([2dc36fd](https://gitlab.com/existe_deja/gitlab-workflow/commit/2dc36fd726738f26ae40d93a331fa095fbc04108))

## [1.3.6](https://gitlab.com/existe_deja/gitlab-workflow/compare/v1.3.5...v1.3.6) (2021-08-25)

## [1.3.5](https://gitlab.com/existe_deja/gitlab-workflow/compare/v1.3.4...v1.3.5) (2021-08-25)


### Bug Fixes

* wording update ([3673648](https://gitlab.com/existe_deja/gitlab-workflow/commit/3673648cdb546ce4a1ac246e2bedd5009a972e73))

## [1.3.4](https://gitlab.com/existe_deja/gitlab-workflow/compare/v1.3.3...v1.3.4) (2021-08-25)

## [1.3.3](https://gitlab.com/existe_deja/gitlab-workflow/compare/v1.3.2...v1.3.3) (2021-08-25)

## [1.3.2](https://gitlab.com/existe_deja/gitlab-workflow/compare/v1.3.1...v1.3.2) (2021-08-25)

## [1.3.1](https://gitlab.com/existe_deja/gitlab-workflow/compare/v1.3.0...v1.3.1) (2021-08-25)

# [1.3.0](https://gitlab.com/existe_deja/gitlab-workflow/compare/v1.2.2...v1.3.0) (2021-08-25)


### Features

* add logo ([a25a809](https://gitlab.com/existe_deja/gitlab-workflow/commit/a25a809d19566262ef2d152da4c2c91404d19710))

## [1.2.2](https://gitlab.com/existe_deja/gitlab-workflow/compare/v1.2.1...v1.2.2) (2021-08-25)


### Bug Fixes

* login to the registry before releasing ([4345bf2](https://gitlab.com/existe_deja/gitlab-workflow/commit/4345bf2609c409859ee3d0280907d6f51d82af28))

## [1.2.1](https://gitlab.com/existe_deja/gitlab-workflow/compare/v1.2.0...v1.2.1) (2021-08-25)


### Bug Fixes

* Dockerfile ([9d2e1b6](https://gitlab.com/existe_deja/gitlab-workflow/commit/9d2e1b61fe4311ea3e951d71856ad5a1a74295c1))

# [1.2.0](https://gitlab.com/existe_deja/gitlab-workflow/compare/v1.1.2...v1.2.0) (2021-08-25)


### Features

* welcome page ([6c16304](https://gitlab.com/existe_deja/gitlab-workflow/commit/6c163041575f9efe928045ef464ba9f781d4db80))

## [1.1.2](https://gitlab.com/existe_deja/gitlab-workflow/compare/v1.1.1...v1.1.2) (2021-08-25)


### Bug Fixes

* wording in french ([9c91a5a](https://gitlab.com/existe_deja/gitlab-workflow/commit/9c91a5ab422a63dca5bc18b1aee9706a8e17bb66))

## [1.1.1](https://gitlab.com/existe_deja/gitlab-workflow/compare/v1.1.0...v1.1.1) (2021-08-19)

# [1.1.0](https://gitlab.com/existe_deja/gitlab-workflow/compare/v1.0.0...v1.1.0) (2021-08-19)


### Bug Fixes

* build with service dind ([c120c61](https://gitlab.com/existe_deja/gitlab-workflow/commit/c120c61f9fa6212f02270fca646716da1c463a2e))
* install node_modules ([5ae6cfc](https://gitlab.com/existe_deja/gitlab-workflow/commit/5ae6cfcf470c0b0e78070a157706ad04c163b250))
* login to the gitlab registry ([2922af4](https://gitlab.com/existe_deja/gitlab-workflow/commit/2922af4dd84d72ba55fe2de496cdbca71e8f3805))
* move copy of package.json ([9f81dba](https://gitlab.com/existe_deja/gitlab-workflow/commit/9f81dba846f2ed6f2e74eb66bfb138a95de5c15d))
* wrong semantic-exec filepath ([19845f8](https://gitlab.com/existe_deja/gitlab-workflow/commit/19845f8e8d0766aefd9813c305f42918df796bb1))


### Features

* serve app through docker ([172662a](https://gitlab.com/existe_deja/gitlab-workflow/commit/172662a7730d2c1e0d0cd38b1b3351cc2174402d))

# 1.0.0 (2021-08-19)


### Bug Fixes

* add package-lock file ([c5c12f8](https://gitlab.com/existe_deja/gitlab-workflow/commit/c5c12f8f90e8f2833bed12b43f47ab15995cda70))


### Features

* sementic release ([183e64e](https://gitlab.com/existe_deja/gitlab-workflow/commit/183e64ebef7d87b38c731e75cd1125af391f8794))
