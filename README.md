# gitlab-workflow

A test repository of dockerising a nuxt application inside a pipeline.

## Manual build

```bash
# replace the CI_REGISTRY_IMAGE by your repo
$ export DOCKER_BUILDKIT=1 CI_REGISTRY_IMAGE=registry.gitlab.com/existe_deja/gitlab-workflow
$ docker build --build-arg BUILDKIT_INLINE_CACHE=1 \
  --file ./docker/Dockerfile \
  --cache-from ${CI_REGISTRY_IMAGE}/base:latest \
  --cache-from ${CI_REGISTRY_IMAGE}/dependencies:latest \
  --cache-from ${CI_REGISTRY_IMAGE}/builder:latest \
  --tag ${CI_REGISTRY_IMAGE}:latest \
  --target prod .
```

## CI pipeline

### The ci image reference
To speed up the builds, I have to add some package that [node-alpine](https://hub.docker.com/_/node/) doesn't include by default.
Since I use [semantic-release](https://semantic-release.gitbook.io/semantic-release/) and my final assets is a docker image I need `git` and `docker`.

Before any pipeline, be sure to upload the correct ci image to the gitlab's container registry.
```bash
# replace the CI_REGISTRY_IMAGE by your repository path
$ export CI_REGISTRY_IMAGE=registry.gitlab.com/existe_deja/gitlab-workflow
$ docker build --build-arg BUILDKIT_INLINE_CACHE=1 \
  --file docker/ci/Dockerfile \
  --cache-from ${CI_REGISTRY_IMAGE}/ci:latest \
  --tag ${CI_REGISTRY_IMAGE}/ci:latest .
# Be sure to login to the gitlab docker registry first (docker login command)
$ docker push ${CI_REGISTRY_IMAGE}/ci:latest
```
**Important**: Since I use a cached image of node alpine, I won't get any updates. To complete a production ready setup I need to rebuild the ci image on a monthly basis. And what's true for the CI is true for the local build.

### The process
First, [semantic-release](https://semantic-release.gitbook.io/semantic-release/) will analyse the commits and decide if a new release should be triggered.
If yes, [@semantic-release/exec](https://github.com/semantic-release/exec) package executes two sh files placed into the `./release` folder.
First I build every layer of the Dockerfile using the [BUILDKIT](https://docs.docker.com/develop/develop-images/build_enhancements/) mode.
It's important to build and push every layers because I'm using Gitlab Docker in Docker service to build the image. Because of this, every pipeline start with an empty cache. Building and pushing all the layers garantee me to use the cache.

Be aware that in your `Dockerfile`, when you `COPY` somethinig, if the content changes then the cache for the next layers is invalidated. This is why I removed the [@semantic-release/npm](https://github.com/semantic-release/npm) package who keeps the version of the app inside the `package.json` file. It always invalidated the cache for package installation.

Get more infos about best practices in the [docker documentation](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/).

## Local build setup (without docker)

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out the [Nuxt documentation](https://nuxtjs.org).
