##############
# base image #
##############
# This is the base image from where we build all others. It has some basic
# configuration for security, cache, optimisation purpose.
FROM node:14.15.5-alpine3.13 as base
# Create app folder
ENV DOCKER_APP_DIR /home/node/app
RUN mkdir -p $DOCKER_APP_DIR && chown -R node:node $DOCKER_APP_DIR
WORKDIR $DOCKER_APP_DIR
# Avoid root user
USER node
COPY --chown=node:node package.json ./

######################
# dependencies image #
######################
FROM base as dependencies
# nuxt-start allows the app to be fully portable, see https://github.com/nuxt/nuxt.js/discussions/9437
RUN npm install "nuxt-start@2.15.7" &> /dev/null
RUN cp -R node_modules prod_node_modules
RUN npm install &> /dev/null

#################
# builder image #
#################
# This is the image who builds the app with all required dependencies. It ends
# with a large disk usage
FROM base as builder
COPY --chown=node:node --from=dependencies $DOCKER_APP_DIR/node_modules ./node_modules
COPY --chown=node:node . .
RUN npm run build --standalone

#############
# app image #
#############
# This is the main image dedicated to serve the app
FROM base AS prod
ENV HOST 0.0.0.0
COPY --chown=node:node --from=dependencies $DOCKER_APP_DIR/prod_node_modules ./node_modules
COPY --chown=node:node --from=builder $DOCKER_APP_DIR/.nuxt ./.nuxt
COPY --chown=node:node --from=builder $DOCKER_APP_DIR/nuxt.config.js ./nuxt.config.js
COPY --chown=node:node --from=builder $DOCKER_APP_DIR/static ./static
EXPOSE 3000
